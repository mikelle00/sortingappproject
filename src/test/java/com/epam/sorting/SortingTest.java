package com.epam.sorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.*;

import java.util.*;

@RunWith(Parameterized.class)
public class SortingTest {

    protected Sorting sorting = new Sorting();
    private final List<Integer> variables;
    private final List<Integer> expectedVariables;

    public SortingTest(List<Integer> variables, List<Integer> expectedVariables) {
        this.variables = variables;
        this.expectedVariables = expectedVariables;
    }

    @Parameterized.Parameters
    public static Object[][] data() {
        //I know there is better way to do it, but I don't have much time for looking other solutions this week :D
        return new Object[][]{
                {new ArrayList<Integer>(){{add(45); add(32);}}, new ArrayList<Integer>(){{add(32); add(45);}}},
                {new ArrayList<Integer>(){{add(4342); add(-45);}}, new ArrayList<Integer>(){{add(-45); add(4342);}}},
                {new ArrayList<Integer>(){{add(-4); add(1); add(-50);}}, new ArrayList<Integer>(){{add(-50); add(-4); add(1);}}},
                {new ArrayList<Integer>(){{add(45); add(-423); add(11);}}, new ArrayList<Integer>(){{add(-423); add(11); add(45);}}},
                {new ArrayList<Integer>(){{add(32); add(512); add(1302); add(431);}}, new ArrayList<Integer>(){{add(32); add(431); add(512); add(1302);}}},
                {new ArrayList<Integer>(){{add(523); add(12); add(674); add(43);}}, new ArrayList<Integer>(){{add(12); add(43); add(523); add(674);}}},
                {new ArrayList<Integer>(){{add(11); add(21); add(31); add(-45); add(66);}}, new ArrayList<Integer>(){{add(-45); add(11); add(21); add(31); add(66);}}},
                {new ArrayList<Integer>(){{add(6545); add(34); add(6456); add(12); add(12312);}}, new ArrayList<Integer>(){{add(12); add(34); add(6456); add(6545); add(12312);}}},
                {new ArrayList<Integer>(){{add(12); add(13); add(321); add(211); add(32); add(78);}}, new ArrayList<Integer>(){{add(12); add(13); add(32); add(78); add(211); add(321);}}},
                {new ArrayList<Integer>(){{add(43); add(123); add(5643); add(234325); add(523); add(645);}}, new ArrayList<Integer>(){{add(43); add(123); add(523); add(645); add(5643); add(234325);}}},
                {new ArrayList<Integer>(){{add(11); add(321); add(-532); add(-54); add(23); add(-53); add(7);}},new ArrayList<Integer>(){{add(-532); add(-54); add(-53); add(7); add(11); add(23); add(321);}}},
                {new ArrayList<Integer>(){{add(523); add(-524); add(4546); add(-76); add(76); add(-967); add(65);}}, new ArrayList<Integer>(){{add(-967); add(-524); add(-76); add(65); add(76); add(523); add(4546);}}},
                {new ArrayList<Integer>(){{add(123); add(432); add(-63); add(-545); add(234); add(4); add(-898); add(65);}}, new ArrayList<Integer>(){{add(-898); add(-545); add(-63); add(4); add(65); add(123); add(234); add(432);}}},
                {new ArrayList<Integer>(){{add(765); add(345); add(545); add(523); add(-43); add(-65); add(-675); add(89);}}, new ArrayList<Integer>(){{add(-675); add(-65); add(-43); add(89); add(345); add(523); add(545); add(765);}}},
                {new ArrayList<Integer>(){{add(3213); add(312321); add(-5423); add(-54); add(23); add(545); add(646); add(87); add(-1);}}, new ArrayList<Integer>(){{add(-5423); add(-54); add(-1); add(23); add(87); add(545); add(646); add(3213); add(312321);}}},
                {new ArrayList<Integer>(){{add(65634); add(-98); add(-67); add(345); add(-6546); add(435); add(123); add(43); add(534);}}, new ArrayList<Integer>(){{add(-6546); add(-98); add(-67); add(43); add(123); add(345); add(435); add(534); add(65634);}}},
                {new ArrayList<Integer>(){{add(656); add(523); add(123); add(66); add(87); add(-345); add(-675); add(-234); add(45); add(-56);}}, new ArrayList<Integer>(){{add(-675); add(-345); add(-234); add(-56); add(45); add(66); add(87); add(123); add(523); add(656);}}},
                {new ArrayList<Integer>(){{add(765); add(8787); add(23); add(-45); add(-45); add(12); add(12); add(654); add(34); add(-787);}}, new ArrayList<Integer>(){{add(-787); add(-45); add(-45); add(12); add(12); add(23); add(34); add(654); add(765); add(8787);}}}
        };
    }

    @Test
    public void sortingArgs() {
        assertEquals(expectedVariables, sorting.sortingMethod(variables));
    }
}