package com.epam.classes;

import com.epam.sorting.Sorting;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class CornerCases {
    Sorting sorting = new Sorting();
    private final int expectedCorrectAmountArgs;

    public CornerCases(int expectedCorrectAmountArgs) {
        this.expectedCorrectAmountArgs = expectedCorrectAmountArgs;
    }

    @Parameterized.Parameters
    public static Object[][] data() {
        return new Object[][]{{13}, {45}, {64}, {97}, {11}, {65434}};
    }

    @Test(expected = IllegalArgumentException.class)
    public void zeroCase() {
        sorting.correctAmountArgs(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void oneCase() {
        sorting.correctAmountArgs(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tenCase() {
        sorting.correctAmountArgs(10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tenMoreCase() {
        sorting.correctAmountArgs(expectedCorrectAmountArgs);
    }
}