package com.epam.sorting;

import java.util.Collections;
import java.util.List;

public class Sorting {
    public List<Integer> sortingMethod(List<Integer> variables) {
        //just to have another class
        Collections.sort(variables);
        return variables;
    }

    public void correctAmountArgs(int argumentsAmount) {
        if (argumentsAmount <= 1 || argumentsAmount >= 10)
            throw new IllegalArgumentException("Arguments from 2 to 10 can only be sorted");
    }
}
