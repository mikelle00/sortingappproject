package com.epam.classes;

import com.epam.sorting.Sorting;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Sorting sorting = new Sorting();

        System.out.println("How many arguments to be sorted?: ");
        int argumentsAmount = scanner.nextInt();
        sorting.correctAmountArgs(argumentsAmount);

        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < argumentsAmount; i++) {
            System.out.println("Type " + i + " variable: ");
            list.add(scanner.nextInt());
        }

        System.out.println("Sorted arguments: " + sorting.sortingMethod(list));
    }



}
